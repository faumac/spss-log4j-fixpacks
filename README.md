# SPSS log4j fixpacks

**Update 2022-01-18: New Version with Log4j 2.17.1 (2022-01-18 @ 16:00).**

IBM says SPSS is not vulnerable to CVE-2021-44832, so.. to stay on 2.17.0 should be fine. However, like IBM, we are providing new interim fixes including Log4j 2.17.1.

----

**Update 2021-12-28: New Version with Log4j 2.17.0 (2021-12-28 @ 23:31).**

Yep, you have to install the fix again! 🤕  
And yes, the new installers also removes all old log4j files 🎉

----

**Update 2021-12-17: New Version with Log4j 2.16.0 (2021-12-17 @ 20:41).**

_____

### macOS for version
* [SPSS 28.0.1.0](https://gitlab.rrze.fau.de/faumac/spss-log4j-fixpacks/-/blob/main/macOS/SPSS-28.0.1.0%20-%20log4j%20fix.pkg)
* [SPSS 27.0.1.0](https://gitlab.rrze.fau.de/faumac/spss-log4j-fixpacks/-/blob/main/macOS/SPSS-27.0.1.0%20-%20log4j%20fix.pkg)
* [SPSS 26.0.0.1](https://gitlab.rrze.fau.de/faumac/spss-log4j-fixpacks/-/blob/main/macOS/SPSS-26.0.0.1%20-%20log4j%20fix.pkg)
* [SPSS 25.0.0.2](https://gitlab.rrze.fau.de/faumac/spss-log4j-fixpacks/-/blob/main/macOS/SPSS-25.0.0.2%20-%20log4j%20fix.pkg) *(for the MacAdmins community)*
### Windows for version
* [SPSS 28.0.0.0](https://gitlab.rrze.fau.de/faumac/spss-log4j-fixpacks/-/blob/main/Windows/rzinst-IBM-SPSS_Log4J28-1.3-W10-x86_x64-ML-fau-3.0.exe)
* [SPSS 27.0.0.0](https://gitlab.rrze.fau.de/faumac/spss-log4j-fixpacks/-/blob/main/Windows/rzinst-IBM-SPSS_Log4J27-1.3-W10-x86_x64-ML-fau-3.0.exe)
* [SPSS 26.0.0.0](https://gitlab.rrze.fau.de/faumac/spss-log4j-fixpacks/-/blob/main/Windows/rzinst-IBM-SPSS_Log4J26-1.3-W10-x86_x64-ML-fau-3.0.exe)
* For other SPSS versions on Windows you have to [fix manually with the SPSS fix files](https://gitlab.rrze.fau.de/faumac/spss-log4j-fixpacks/-/tree/main/IBM%20files)  

## Install on macOS (pkg signed)
1) Right click on the .pkg (i.g. with "control" + click)
2) Choose "open"
3) Follow the provided steps

![]( ./macOS_help.gif)

## Install on windows
1) Open .exe with admin rights
2) Follow the provided steps

## Why? 
..bc IBM only provides stupid zips with a ~~extremly~~ bad readme..   
We provide these installers without any guarantee

Also thanks to @grahamrpugh (one missing path in 28.0.1.0 mac) and @hcodfrie (for log4j2.16 IBM SPSS notice)
